import React from "react";
import imgProducto from "./assets/product1.jpg";

// Componente para mostrar los detalles del producto
const ProductDetail = ({ product }) => {
  return (
    <div className="product-detail card bg-light p-1">
      <div className="row g-0">
        <div className="col-12 col-md-6 d-flex justify-content-center align-items-center">
          <a href="#">
            <img
              src={imgProducto}
              className="img-fluid product-image"
              alt={product.name}
            />
          </a>
        </div>
        <div className="col-12 col-md-6 p-3">
          <small className="text-muted">SKU: {product.sku}</small>
          <h1 className="product-name">{product.name}</h1>
          <p className="lead">{product.description}</p>
          <p className="product-price text-success display-4">
            $ {product.price} ARS
          </p>
          <p className="text-secondary">
            Cantidad disponible: {product.quantity}
          </p>
          <button className="btn btn-primary">Comprar</button>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
