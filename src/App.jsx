import "./App.css";
import ProductDetail from "./ProductDetail";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  const producto = {
    name: "Paleta Siux Pegasus",
    description:
      "La Siux Pegasus es una pala de pádel de alta gama que está diseñada para ofrecer un juego polivalente. Está fabricada con materiales de calidad superior, como fibra de carbono y grafeno en las caras para aumentar su resistencia y durabilidad. El marco de carbono con doble tubular y puente en forma de rombo amplía el punto dulce y reduce las vibraciones. El núcleo de goma Eva Soft Laminada proporciona una potencia sobresaliente. La forma de lágrima (gota invertida) y el balance medio la convierten en una pala ideal para un juego polivalente.",
    price: "800.000",
    sku: "532592024",
    quantity: "5",
  };

  return (
    <>
      <div className="mt-5">
        <ProductDetail product={producto} />
      </div>
    </>
  );
}

export default App;
